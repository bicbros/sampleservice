package com.bic.service.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bic.service.domain.Employee;
import com.bic.service.repository.EmployeeDao;

@Controller
@RequestMapping("/hello")
public class Home {
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public String sayHello() {

		return "Service alive!";

	}
	
	@RequestMapping(value="/{name}", method = RequestMethod.GET)
	public String getMovie(@PathVariable String name, ModelMap model) {

		model.addAttribute("movie", name);
		return "list";

	}
	
	@Autowired
	private EmployeeDao employeeDao;

	@RequestMapping(value="employee/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Employee getEmployee(@PathVariable int id) {

		return employeeDao.getById(id);

	}
}
