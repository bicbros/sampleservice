package com.bic.service.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.bic.service.repository.EmployeeDao;
import com.bic.service.repository.JdbcEmployeeDao;

@Configuration
public class AppConfig {
	
	@Value("#{properties.username}")
	private String username;
	
	@Value("#{properties.password}")
	private String password;
	
	@Value("#{properties.url}")
	private String url;
	
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUsername(username);
		ds.setPassword(password);
		ds.setUrl(url);
		return ds;
	}

	@Bean
	public EmployeeDao employeeDao() {
		return new JdbcEmployeeDao(dataSource());
	}
}
