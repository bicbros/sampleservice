package com.bic.service.repository;

import com.bic.service.domain.Employee;

public interface EmployeeDao {

	Employee getById(int employeeId);
}
