package com.bic.service.repository;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.bic.service.domain.Employee;

@Repository
public class JdbcEmployeeDao implements EmployeeDao {

	@Value("#{sqlProperties.getEmployeeBydId}")
	private String query;
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public JdbcEmployeeDao(){
	}
	
	public JdbcEmployeeDao(DataSource dataSource) {
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				dataSource);
	}

	public Employee getById(int employeeId) {

		SqlParameterSource namedParameters = new MapSqlParameterSource(
				"employeeId", employeeId);

		return namedParameterJdbcTemplate.queryForObject(query,
				namedParameters, new EmployeeRowMapper());
	}
}
