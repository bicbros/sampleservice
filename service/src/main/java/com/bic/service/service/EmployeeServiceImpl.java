package com.bic.service.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.bic.service.domain.Employee;
import com.bic.service.repository.EmployeeDao;

public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	
	public Employee getById(int employeeId) {
		return employeeDao.getById(employeeId);
	}

}
