package com.bic.service.service;

import com.bic.service.domain.Employee;

public interface EmployeeService {

	Employee getById(int employeeId);
}
